import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button,
    ListView,
    TextInput,
    ScrollView
  } from 'react-native';
import DeleteDishRequest from '../requests/Delete';
import UpdateDishRequest from '../requests/UpdateDish';
import { Dimensions } from "react-native";

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
export default class DishView extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            showUpdate: false,
            newName: null,
            newDescription: null,
            newVenue: null,
        }

        this.delete = this.delete.bind(this);
    }

    render() {
        return(
            <ScrollView style={{ padding:10, width:width}}>
                {
                    !this.state.showUpdate ? (
                        <View>
                            <Text>{'Name: '} {this.props.name}</Text>
                            <Text>{'Description: '} {this.props.description}</Text>
                            <Text>{'Venue: '} {this.props.venue}</Text>
                            <Button 
                                onPress={this.delete}
                                title={'Delete'}
                            />
                            <Button 
                                onPress={()=>{this.setState({showUpdate:true})}}
                                title={'Update'}
                            />
                        </View>
                    ) : (
                        <ScrollView>
                            <Text>{'Name'}</Text>
                            <TextInput
                                onChangeText={(text)=>{this.setState({newName:text})}}
                                value={this.state.newName !== null ? this.state.newName : this.props.name}
                            />
                            <Text>{'Description'}</Text>
                            <TextInput
                                onChangeText={(text)=>{this.setState({newDescription:text})}}
                                value={this.state.newDescription !== null ? this.state.newDescription : this.props.description}
                            />
                            <Text>{'Venue'}</Text>
                            <TextInput
                                onChangeText={(text)=>{this.setState({newVenue:text})}}
                                value={this.state.newVenue !== null ? this.state.newVenue : this.props.venue}
                            />
                            <Button
                                onPress={this.onUpdateDish}
                                title={'Execute update'}
                            />
                        </ScrollView>
                    )
                }
            </ScrollView>
        );
    }

    onUpdateDish = () => {
        const dish = {
            id: this.props.dishId,
            name: this.state.newName,
            description: this.state.newDescription,
            venue: this.props.venue,
        };

        this.setState({showUpdate:false},()=>{this.props.updateDishInStore(dish);});
    }

    delete() {
        this.props.removeDishFromStore(this.props.dishId);
    }
}