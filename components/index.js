import React from 'react';
import {
    StyleSheet,
    View,
    Button,
    ListView,
    Text,
    TextInput
  } from 'react-native';
import { connect } from 'react-redux';
import { addDish, getDishes, updateDish, removeDish } from '../actions/actions';
import {logout} from '../actions/actions';
import LogoutRequest from '../requests/Logout';
import DishView from './DishView';
import GetDishesRequest from '../requests/GetDishes';
import AddDishRequest from '../requests/Add';
import SyncDishesRequest from '../requests/SyncDishes';
import { Container, Header, Content, List, ListItem, Footer } from 'native-base';
import { Dimensions } from "react-native";

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class DishesComponent extends React.Component {
    
    constructor(props){
        super(props);

        this.state = {
            showAdd: false,
            newName: null,
            newDescription: null,
            newVenue: null,

        };

        this.logout = this.logout.bind(this);
    }

    render(){
        return(
                     
          <Container>
            <Content  style={{ padding:10, width:width }}
              contentContainerStyle={{ justifyContent: 'center', flex: 1 }}>
                {
                    !this.state.showAdd ?
                    (
                        
                        
                            this.renderDishes()
                        
                      
                    ) :
                    (

                        <View style={{backgroundColor:'green', padding:10, width:200}}>
                            <Text>{'Name'}</Text>
                            <TextInput
                                onChangeText={(text)=>{this.setState({newName:text})}}
                                value={this.state.newName}
                            />
                            <Text>{'Description'}</Text>
                            <TextInput
                                onChangeText={(text)=>{this.setState({newDescription:text})}}
                                value={this.state.newDescription}
                            />
                            <Text>{'Venue'}</Text>
                            <TextInput
                                onChangeText={(text)=>{this.setState({newVenue:text})}}
                                value={this.state.newVenue}
                            />
                            <Button
                                onPress={this.onNewDishAdded}
                                title={'Add new dish'}
                            />
                        </View>
                    )
                    
                }
                </Content>
        <Footer>
            <Button
                    style={{width: 50}}
                    onPress={this.logout}
                    title={"Log out"}
                />
                <Button
                            onPress={()=>{this.setState({showAdd:true})}}
                            title={'Add dish'}
                        />
                </Footer>
            </Container>
        );
    }

    renderDishes() {
        console.log(this.props.dishes);
        const dishes = [];
        if(this.props.dishes !== undefined){
            for(let i=0;i<this.props.dishes.length;i++){
                dishes.push(
                    <DishView
                        key={i}
                        // userId={this.props.security.userId}
                        dishId={this.props.dishes[i].id}
                        name={this.props.dishes[i].name}
                        description={this.props.dishes[i].description}
                        venue={this.props.dishes[i].venue}

                        updateDishInStore={(dish)=>{this.setState(()=>{
                            this.props.onUpdateDish(dish);
                            setTimeout(()=>{
                                this.syncDishes();
                            }, 400);
                        })}}
                        removeDishFromStore={(dishId)=>{this.setState(()=>{
                            this.props.onRemoveDish(dishId);
                            setTimeout(()=>{
                                this.syncDishes();
                            }, 400);
                        })}}
                        syncDishes={this.syncDishes}
                    />
                );
            }
        }
        return dishes;
    }

    syncDishes = () => {
        new SyncDishesRequest(
            this.props.security.userId,
            this.props.dishes,
            ()=>{
                new GetDishesRequest((dishes)=>{
                    this.props.onGetDishes(dishes);
                }).send();
            },
            ()=>{
                // nothing atm
            }
        ).send();
    }

    onNewDishAdded = () => {
        const dish = {
            id: new Date().getUTCMilliseconds(),
            name: this.state.newName,
            description: this.state.newDescription,
            venue: this.state.newVenue
        }

        const onSuccessfullDishAdded = () => {
            new GetDishesRequest((dishes)=>{
                this.props.onGetDishes(dishes);
            },this.props.security.userId).send();
            this.setState({showAdd:false});
        }

        const onFailedDishAdded = () => {
            new GetDishesRequest((dishes)=>{
                this.props.onGetDishes(dishes);
            },this.props.security.userId).send();
            this.setState({showAdd:false});
        }

        this.setState({showAdd:false},()=>{this.props.onAddDish(dish);});
        setTimeout(() => {
            this.syncDishes();
        }, 400);
    }

    logout() {
        new LogoutRequest(
            () => {
                this.props.onLogout();
                this.props.forceUpdateParent();
            }, this.props.security.token,
        ).send();
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddDish: dish => {
            dispatch(addDish(dish));
        },
        onGetDishes: dishes => {
            dispatch(getDishes(dishes));
        },
        onRemoveDish: dishId => {
            dispatch(removeDish(dishId));
        },
        onUpdateDish: dish => {
            dispatch(updateDish(dish));
        },
        onLogout: () => {
            dispatch(logout());
        }
    };
};

const mapStateToProps = state => ({
    dishes: state.dishes,
    security: state.security,
});

export default connect(mapStateToProps,mapDispatchToProps)(DishesComponent);